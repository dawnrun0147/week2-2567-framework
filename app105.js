const me = () => 
    ({
        name: "Mr.Mark",
        age: 25
    });

const display = me();
console.log(display);